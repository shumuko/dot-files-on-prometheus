### Added for rbenv
eval "$(rbenv init -)"
export PATH=/usr/local/bin:$PATH

### Added by the Heroku Toolbelt
export PATH="/usr/local/heroku/bin:$PATH"
source "`brew --prefix grc`/etc/grc.bashrc"

### Added for color ls and grep
export CLICOLOR=1
export GREP_OPTIONS='--color=auto'

### For git tab completion
if [ -f `brew --prefix`/etc/bash_completion ]; then
      . `brew --prefix`/etc/bash_completion
fi

### For tmux and git branch path
# PS1="$PS1"'$([ -n "$TMUX" ] && tmux setenv TMUXPWD_$(tmux display -p "#D" | tr -d %) "$PWD")'
source ~/.git-prompt.sh
GIT_PS1_SHOWDIRTYSTATE=1 # Show statge changes
PROMPT_DIRTRIM=1
PS1='\[\e[0;34m\]\h:\[\e[m\]\w \[\e[0;33m\]$(__git_ps1 "[%s]")\[\e[m\]\$ ''$([ -n "$TMUX" ] && tmux setenv TMUXPWD_$(tmux display -p "#D" | tr -d %) "$PWD")'

# Aliases
alias be="bundle exec"
alias ll="ls -la"
alias fbe="foreman run bundle exec"
alias giti="git log --graph --oneline --all --decorate"
alias gru="git remote update"
alias cd..="cd .."
alias ..="cd.."
alias grep="grep --color=auto"
alias r="rbenv"
alias vag="vagrant"

# History length
HISTFILESIZE=25000

# Docker
export DOCKER_HOST=tcp://192.168.59.103:2375
